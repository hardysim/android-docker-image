FROM openjdk:8-jdk
MAINTAINER Simon Hardt <info@nullcode.de>

ENV ANDROID_TARGET_SDK="25" \
    ANDROID_BUILD_TOOLS="25.0.2" \
    SDK_TOOLS="25.2.3"

RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1

RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/tools_r${SDK_TOOLS}-linux.zip && \
     unzip android-sdk.zip

RUN echo y | tools/android --silent update sdk --no-ui --all --filter android-${ANDROID_TARGET_SDK} && \
    echo y | tools/android --silent update sdk --no-ui --all --filter platform-tools && \
    echo y | tools/android --silent update sdk --no-ui --all --filter build-tools-${ANDROID_BUILD_TOOLS}

RUN echo y | tools/android --silent update sdk --no-ui --all --filter extra-android-m2repository && \
    echo y | tools/android --silent update sdk --no-ui --all --filter extra-google-google_play_services && \
    echo y | tools/android --silent update sdk --no-ui --all --filter extra-google-m2repository

ENV ANDROID_HOME $PWD